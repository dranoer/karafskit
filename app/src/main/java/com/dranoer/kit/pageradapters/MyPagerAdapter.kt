package com.dranoer.kit.pageradapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.dranoer.kit.fragments.FirstFragment
import com.dranoer.kit.fragments.SecondFragment

class MyPagerAdapter(fm: FragmentManager?) : FragmentStatePagerAdapter(fm) {

    val titles = arrayOf("HOME", "OFFICE", "HOLIDAY", "ACTIVITIES")

    override fun getItem(position: Int): Fragment {
        lateinit var fragment: Fragment
        when (position) {
            0 -> fragment = FirstFragment()
            1 -> fragment = SecondFragment()
            2 -> fragment = FirstFragment()
            3 -> fragment = FirstFragment()
        }
        return fragment
    }

    override fun getCount(): Int {
        return titles.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }
}