package com.dranoer.kit.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v4.util.Pair
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dranoer.kit.R
import com.dranoer.kit.activity.SecondDetailActivity
import com.dranoer.kit.activity.MainActivity
import com.dranoer.kit.adapters.LargeImageItemClickListener
import com.dranoer.kit.adapters.LargeImageListAdapter
import com.dranoer.kit.models.Item
import kotlinx.android.synthetic.main.activity_second_detail.*
import kotlinx.android.synthetic.main.fragment_second.*

class SecondFragment : Fragment(), LargeImageItemClickListener {

    // Add View to Shared Elements :)
    override fun onItemClick(item: Item, imageView: ImageView, textView: TextView) {
        val detailIntent = Intent(mActivity, SecondDetailActivity::class.java)
        val imageViewPair = Pair.create<View, String>(imageView, getString(R.string.image_transition_name))
        val textViewPair = Pair.create<View, String>(textView, getString(R.string.text_transition_name))
//        val cardView = Pair.create<View, String>(card_view_third, getString(R.string.card_transition_name))
//        val fab = Pair.create<View, String>(fab, getString(R.string.fab_transition_name))
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity, imageViewPair, textViewPair)
        detailIntent.putExtra(SecondDetailActivity.DATA, item)
        startActivity(detailIntent, options.toBundle())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_second, container, false)
        return view
    }

    lateinit var mAdapter: LargeImageListAdapter
    //activity instance
    lateinit var mActivity: AppCompatActivity

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mActivity = activity as MainActivity

        mAdapter = LargeImageListAdapter(mActivity, this)
        rvLargeImageItems.layoutManager = LinearLayoutManager(mActivity)
        rvLargeImageItems.adapter = mAdapter

        mAdapter.setItems(Item.ITEMS.toList())
    }
}