package com.dranoer.kit.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.dranoer.kit.R
import com.dranoer.kit.extensions.isNetworkAvailable
import com.dranoer.kit.extensions.toast
import com.dranoer.kit.pageradapters.MyPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val pagerAdapter = MyPagerAdapter(supportFragmentManager)
        viewPager.adapter = pagerAdapter
        tabLayout.setupWithViewPager(viewPager)

        if (!isNetworkAvailable()) {
            toast(getString(R.string.connection_error), Toast.LENGTH_SHORT)
        }
    }
}